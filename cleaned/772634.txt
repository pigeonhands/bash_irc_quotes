        #772634 +(1701)- [X]

        <Sollen> so were walking back to the lab I m distracted and
        paying attention that were coming up to the street and I go to
        put my foot down and the curb isn't there any more.
        <Sollen> and I actually realize I'm starting to fall, and I
        could of tried to stumble and keep walking; except I have my
        laptop in my bookbag and after the trouble I had I'm not about
        to risk banging it around.  So my geek impulses say "save the
        computer; sacrifice your body"
        <Sollen> I guess all the martial arts helps because I managed
        to do this nice slow controlled fall to the ground, at the
        last minute I let go of my soda to throw my hand to the ground
        and somehow the soda  manages to fall  so it's standing face
        up without spilling a drop.
        <Sollen> so there I am lying in the middle of this road in
        center of campus with everyone staring at me after I just did
        this perfect drop from standing position to lying position
        completely uninjured without spilling a drop
        <Sollen> so I decide to play it cool, I just lie there like
        "yeah I meant to do that, I was just getting tired so I
        thought I would take a nap right here"

